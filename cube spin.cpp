#include<GL/glut.h>
float vertices[] = {-1,-1,-1,1,-1,-1,1,1,-1,-1,1,-1,-1,-1,1,1,-1,1,1,1,1,-1,1,1};
float colors[] = {1,0,0,0,1,0,0,0,1,1,1,0,0,1,1,1,0,1};
GLubyte indices[] = {0,1,2,3,4,5,6,7,5,1,2,6,0,4,7,3,0,1,5,4,7,6,3,2};
float theta[] = {0,0,0};
int axis = 1;
void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glRotatef(theta[0],1,0,0);
    glRotatef(theta[1],0,1,0);
    glRotatef(theta[2],0,0,1);
    glDrawElements(GL_QUADS,24,GL_UNSIGNED_BYTE,indices);               //I have used glDrawElements, in case the teacher doesn't agree do it the hard way by defining cube points 
    glFlush();                                                          //for each face and colors for each face and GL_QUADS and glRotatef.
}
void spin()
{
    theta[axis] +=0.025;
    glutPostRedisplay();
}
void mouse(int button,int state,int x,int y)
{
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
        axis = 1;
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
        axis = 2;
    if(button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
        axis = 3;

}
void init()
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-2,2,-2,2,-2,2);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(500,500);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Cube Spin");
    init();
    glutDisplayFunc(display);
    glutMouseFunc(mouse);
    glutIdleFunc(spin);

    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(3,GL_FLOAT,0,colors);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3,GL_FLOAT,0,vertices);

    glutMainLoop();
}
