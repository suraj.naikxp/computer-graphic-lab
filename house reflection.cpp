#define _USE_MATH_DEFINES               //Used to obtain value of Pi directly
#include<cmath>
#include<GL/glut.h>
#include<iostream>
using namespace std;
float m,c;
void house()
{
    glBegin(GL_LINE_LOOP);
    glVertex2d(100,100);
    glVertex2d(100,300);
    glVertex2d(200,300);
    glVertex2d(200,100);
    glEnd();
    glBegin(GL_LINE_LOOP);
    glVertex2d(125,100);
    glVertex2d(125,200);
    glVertex2d(175,200);
    glVertex2d(175,100);
    glEnd();
    glBegin(GL_LINE_LOOP);
    glVertex2d(100,300);
    glVertex2d(150,400);
    glVertex2d(200,300);
    glEnd();
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,0,0);
    glPushMatrix();
    house();
    glTranslatef(0,c,0);
    glRotatef(m,0,0,1);
    glScalef(1,-1,1);
    glRotatef(-m,0,0,1);
    glTranslatef(0,-c,0);
    house();
    glFlush();
    glPopMatrix();
}
void init()
{
    glClearColor(1,1,1,1);
    glMatrixMode(GL_POLYGON);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    cout<<"Enter m,c"<<endl;
    cin>>m>>c;
    m = atan(m)*180/M_PI;                                   //M_PI comes from _USE_MATH_DEFINES, directly use 3.14 if you feel like
    glutCreateWindow("Reflection");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
