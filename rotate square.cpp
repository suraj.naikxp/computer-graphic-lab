#include<GL/glut.h>
#include<iostream>
#include<cmath>
using namespace std;
int single_window,double_window;
float x,y;
double rad = 0.0;
void spin()
{
    rad+=0.025;
    x = cos(rad);
    y = sin(rad);

    glutSetWindow(single_window);
    glutPostRedisplay();
    glutSetWindow(double_window);
    glutPostRedisplay();
}
void draw()
{
    glBegin(GL_QUADS);
    glColor3f(1,0,0);
    glVertex2f(x,y);
    glColor3f(0,1,0);
    glVertex2f(-y,x);
    glColor3f(0,0,1);
    glVertex2f(-x,-y);
    glColor3f(1,1,0);
    glVertex2f(y,-x);
    glEnd();
}
void single_display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    draw();
    glFlush();
}
void double_display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    draw();
    glutSwapBuffers();
}
void myMouse(int button,int state,int x,int y)
{
    if(button == GLUT_LEFT_BUTTON && state==GLUT_DOWN)
    {
        glutIdleFunc(spin);
    }
    else if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
    {
        glutIdleFunc(NULL);
    }
}
void init()
{
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-2,2,-2,2);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);

    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    single_window = glutCreateWindow("Single Buffer");
    init();
    glutIdleFunc(spin);
    glutMouseFunc(myMouse);
    glutDisplayFunc(single_display);

    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(600,0);
    double_window = glutCreateWindow("Double Buffer");
    init();
    glutIdleFunc(spin);
    glutMouseFunc(myMouse);
    glutDisplayFunc(double_display);
    glutMainLoop();
}
