#include<GL/glut.h>
#include<iostream>
using namespace std;
int xc,yc,r;
void showPoint(int x,int y)
{
    glBegin(GL_POINTS);
    glVertex2i(xc+x,yc+y);
    glVertex2i(xc+x,yc-y);
    glVertex2i(xc-x,yc+y);
    glVertex2i(xc-x,yc-y);

    glVertex2i(xc+y,yc+x);
    glVertex2i(xc+y,yc-x);
    glVertex2i(xc-y,yc+x);
    glVertex2i(xc-y,yc-x);
    glEnd();
}
void display()
{
    int x = 0,y=r;
    int d = 3 - 2*r;
    while(x<=y)
    {
        showPoint(x,y);
        x++;
        if(d<0)
        {
            d = d+ 4*x + 6;
        }
        else
        {
            y--;
            d = d + 4*x - 4*y + 10;
        }
        showPoint(x,y);
    }
    glFlush();
}
void init()
{
    glColor3f(0,0,0);
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    cin>>xc>>yc>>r;
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Hello World");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
