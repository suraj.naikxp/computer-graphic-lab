#include<GL/glut.h>
#define CAR 1
void draw_wheel()
{
    glColor3f(0,1,0);
    glutSolidSphere(10,25,25);
}
void carlist()
{
    glNewList(CAR,GL_COMPILE);
    glColor3f(0,1,1);
    glBegin(GL_POLYGON);
    glVertex3f(0,25,0);
    glVertex3f(90,25,0);
    glVertex3f(90,55,0);
    glVertex3f(80,55,0);
    glVertex3f(80,75,0);
    glVertex3f(20,75,0);
    glVertex3f(20,55,0);
    glVertex3f(0,55,0);
    glEnd();
    glEndList();
}
void keyboard(unsigned char key,int x,int y)
{
    if(key == 't')
        glutPostRedisplay();
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    carlist();
    glTranslatef(1,0,0);
    glCallList(CAR);

    glPushMatrix();
    glTranslatef(25,25,0);
    draw_wheel();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(75,25,0);
    draw_wheel();
    glPopMatrix();

    glFlush();
}
void init()
{
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,600,0,600,0,600);                 //Reduce to a lower value to magnify the car
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Car");
    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
}
