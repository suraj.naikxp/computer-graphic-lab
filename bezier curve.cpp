#include<GL/glut.h>
#include<iostream>
#include<cmath>
float xt[4],yt[4];
int cnt=0;
void myDisplay()
{
    float x,y;
    if(cnt!=4)
        return;
    for(double i=0;i<=1.0;i+=0.0005)
    {
        x = pow(1-i,3) * xt[0] + 3*pow(1-i,2)*i*xt[1] + 3*(1-i)*pow(i,2)*xt[2]+pow(i,3)*xt[3];
        y = pow(1-i,3) * yt[0] + 3*pow(1-i,2)*i*yt[1] + 3*(1-i)*pow(i,2)*yt[2]+pow(i,3)*yt[3];
        glBegin(GL_POINTS);
        glVertex2f(x,y);
        glEnd();
        glFlush();
    }
}
void myMouse(int button,int state,int x,int y)         
{
    y = 600 - y;
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        ++cnt;
        if(cnt==1)
        {
            xt[0]=x;yt[0]=y;
            glBegin(GL_POINTS);
            glVertex2f(xt[0],yt[0]);
            glEnd();
            glFlush();
        }
        if(cnt==2)
        {
            xt[1]=x;yt[1]=y;
            glBegin(GL_POINTS);
            glVertex2f(xt[1],yt[1]);
            glEnd();
            glFlush();
        }
        if(cnt==3)
        {
            xt[2]=x;yt[2]=y;
            glBegin(GL_POINTS);
            glVertex2f(xt[2],yt[2]);
            glEnd();
            glFlush();
        }
        if(cnt==4)
        {
            xt[3]=x;yt[3]=y;
            glBegin(GL_POINTS);
            glVertex2f(xt[3],yt[3]);
            glEnd();
            glFlush();
            glutPostRedisplay();
        }
        if(cnt>4)
        {
            xt[0] = xt[3];
            yt[0] = yt[3];
            cnt = 1;
            glBegin(GL_POINTS);
            glVertex2f(xt[0],yt[0]);
            glEnd();
            glFlush();
        }
    }
}
void init()
{
    glColor3f(1,0,0);
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Bezier Curve");
    init();
    glutDisplayFunc(myDisplay);
    glutMouseFunc(myMouse);
    glutMainLoop();
    return 0;
}
