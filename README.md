# Computer Graphics Lab

This repo contains codes of computer graphics lab course [12CS72] of 7th semester CSE course of RVCE. 

The codes for this repository comes from various authors. 
The difficult programs [Cohen Hodgeman and Scan Line] comes from Microsoft's very own Srinivas S T [https://www.linkedin.com/in/srinivas-thimmaiah-581490129/]

A few other codes comes from various other repositories like [https://github.com/nowke/cg_lab] either edited by me to make it more user friendly or used as it is.

A few of programs are written by me :).