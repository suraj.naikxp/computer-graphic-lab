#include<iostream>
#include<GL/glut.h>
using namespace std;
float vertex[4][3] = {{0,200,400},{0,0,-350},{-300,300,400},{200,300,350}};
int n=2;
void draw(float* a,float* b,float* c)
{
    glBegin(GL_TRIANGLES);
    glVertex3fv(a);
    glVertex3fv(b);
    glVertex3fv(c);
    glEnd();
}
void divideTriangle(float* A,float* B,float* C,int m)
{
    if(m>0)
    {
        float mid[3][3];
        for(int i=0;i<3;i++)
        {
            mid[0][i] = (A[i]+B[i])/2;
            mid[1][i] = (B[i]+C[i])/2;
            mid[2][i] = (C[i]+A[i])/2;
        }
        divideTriangle(A,mid[0],mid[2],m-1);
        divideTriangle(B,mid[0],mid[1],m-1);
        divideTriangle(C,mid[1],mid[2],m-1);
    }
    else
    {
        draw(A,B,C);
    }
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3f(1,0,0);
    divideTriangle(vertex[0],vertex[1],vertex[2],n);
    glColor3f(0,1,0);
    divideTriangle(vertex[0],vertex[2],vertex[3],n);
    glColor3f(0,0,1);
    divideTriangle(vertex[0],vertex[1],vertex[3],n);
    glColor3f(1,1,0);
    divideTriangle(vertex[1],vertex[2],vertex[3],n);

    glFlush();
}
void init()
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(1,1,1,1);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-600,600,-600,600,-600,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    cin>>n;
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_DEPTH);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Hello World");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
