#include<GL/glut.h>
#include<iostream>
using namespace std;
int x,y,theta;
void house()
{
    glBegin(GL_LINE_LOOP);
    glVertex2d(100,100);
    glVertex2d(100,300);
    glVertex2d(200,300);
    glVertex2d(200,100);
    glEnd();
    glBegin(GL_LINE_LOOP);
    glVertex2d(125,100);
    glVertex2d(125,200);
    glVertex2d(175,200);
    glVertex2d(175,100);
    glEnd();
    glBegin(GL_LINE_LOOP);
    glVertex2d(100,300);
    glVertex2d(150,400);
    glVertex2d(200,300);
    glEnd();
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,0,0);
    glPushMatrix();
    house();
    glFlush();
    glTranslated(x,y,0);
    glRotatef(theta,0,0,1);
    glTranslated(-x,-y,0);
    glColor3f(0,0,1);
    house();
    glFlush();
    glPopMatrix();
}
void init()
{
    glClearColor(1,1,1,1);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);

    cout<<"Enter x,y,theta"<<endl;
    cin>>x>>y>>theta;

    glutCreateWindow("Rotation");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
