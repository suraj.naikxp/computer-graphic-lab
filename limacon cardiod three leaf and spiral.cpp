#define _USE_MATH_DEFINES
#include<GL/glut.h>
#include<cmath>
int curveNum;
struct pOINT
{
    int x,y;
};
void line(pOINT p1,pOINT p2)
{
    glBegin(GL_POINTS);
    glVertex2f(p1.x,p1.y);
    glVertex2f(p2.x,p2.y);
    glEnd();
}
void draw(int curveNum)
{
    int a =175;
    int b =60;
    int x1 = 200,y1 = 250;
    double r,theta, dtheta = 1/float(a);
    double twoPi= 2*M_PI;                                   //M_PI comes from _USE_MATH_DEFINES, directly use 3.14 if you feel like
    pOINT curve[2];
    curve[0].x = x1;
    curve[0].y = y1;
    switch(curveNum)
    {
        case 1: curve[0].x = a+b;
                break;
        case 2: curve[0].x = a+a;
                break;
        case 3: curve[0].x = a;
                break;
        case 4: break;
        default: break;
    }
    theta = dtheta;
    while(theta<twoPi)
    {
        switch(curveNum)
        {
            case 1: r = a * cos(theta) + b;
                    break;
            case 2: r = a * cos(theta) + a;
                    break;
            case 3: r = a * cos(3 * theta);
                    break;
            case 4: r = (a/4.0) * theta;
                    break;
            default: break;
        }
        curve[1].x = x1 + r * cos(theta);
        curve[1].y = y1 + r * sin(theta);
        line(curve[0],curve[1]);
        curve[0] = curve[1];
        theta += dtheta;
    }
}
void myDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT);
    draw(curveNum);
    glFlush();
}
void myKeyboard(unsigned char key,int x,int y)
{
    switch(key)
    {
        case '1': curveNum = 1;
                    break;
        case '2': curveNum = 2;
                    break;
        case '3': curveNum = 3;
                    break;
        case '4': curveNum = 4;
                    break;
    }
    glutPostRedisplay();
}
void init()
{
    glColor3f(1,0,0);
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Shapes");
    init();
    glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    glutMainLoop();
    return 0;
}
