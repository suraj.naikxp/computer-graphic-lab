/* The does not follow the text book method, 
    entirely coded by Srinivas S T, he spent a lot
    of time and effort. Huge Shoutout to him.
    */


#include<GL/glut.h>
#include<algorithm>
float points[][2] = {
    {
        100,200
    },
    {
        150,100
    },
    {
        200,250
    },
    {
        300,300
    },
    {
        90,300
    },
    {
        65,400
    }
};
float intx[10] = {0};
int m;
int ymax = 500;
void drawLine(float p1,float p2,int scanline)
{
    glColor3f(1,0,0);
    glBegin(GL_LINES);
    glVertex2f(p1,scanline);
    glVertex2f(p2,scanline);
    glEnd();
    glFlush();
    glFlush();
}
void addIntersect(float p1[2],float p2[2],int scanline)
{
    if(p2[1]<p1[1])
    {
        float* p=p1;
        p1 = p2;
        p2 = p;
    }
    float slope = (p2[1]-p1[1])/(p2[0]-p1[0]);
    if(scanline<=p2[1] && scanline >=p1[1])
    {
        intx[m++] = p1[0] + (scanline -p1[1])/slope;
    }
}
void drawPoly(float p[][2],int n)
{
    glColor3f(0,0,1);
    glBegin(GL_LINE_LOOP);
    for(int i=0;i<n;i++)
    {
        glVertex2fv(p[i]);
    }
    glEnd();
}
void scanFill(float p[][2],int n)
{
    for(int i=0;i<ymax;i++)
    {
        m=0;
        for(int j=0;j<n;j++)
        {
            float n1 = p[j][1] - p[(j+1)%n][1];
            float n2 = p[(j+1)%n][1] - p[(j+2)%n][1];

            if(i!=p[(j+1)%n][1])
            {
                addIntersect(p[j],p[(j+1)%n],i);
            }
            else if(n1*n2<0)
            {
                addIntersect(p[j],p[(j+1)%n],i);
            }
        }
        std::sort(intx,intx+m);
        if(m>=2)
        {
            for(int j=0;j<m;j+=2)
            {
                drawLine(intx[j],intx[j+1],i);
            }
        }
        //for(long j=0;j<10000000;j++);
    }
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    drawPoly(points,7);
    glFlush();
}
void displayFill()
{
    glClear(GL_COLOR_BUFFER_BIT);
    drawPoly(points,7);
    scanFill(points,7);
    glFlush();
}
void init()
{
    glClearColor(1,1,1,1);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Poly");
    init();
    glutDisplayFunc(display);

    glutInitWindowPosition(600,0);
    glutCreateWindow("PolyFill");
    init();
    glutDisplayFunc(displayFill);
    glutMainLoop();
    return 0;
}
