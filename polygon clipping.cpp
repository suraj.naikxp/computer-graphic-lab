/* The code is given to me by Srinivas S T
    Give him a shoutout if you like it.
    */


#include<iostream>
#include<GL/glut.h>
using namespace std;
struct vertex
{
    float x,y;
};
vertex polygon[10];
vertex border[4];
int vcount = 0;
void draw_polygon(vertex* p,int n)
{
    glBegin(GL_POLYGON);                                //When using a concave polygon, GL_POLYGON won't work as GL_POLYGON can't draw concave polygons.
    for(int i=0;i<n;i++)                                //Use GL_LINE_LOOP for concave polygon, for Convex polygons you can use anything.
    {
        glVertex2f(p[i].x,p[i].y);
    }
    glEnd();
}
bool isIn(vertex v,int side)
{
    if(side == 0 && v.x>=border[0].x)
        return true;
    if(side == 1 && v.x<=border[1].x)
        return true;
    if(side == 2 && v.y>=border[0].y)
        return true;
    if(side == 3 && v.y<=border[2].y)
        return true;
    return false;
}
vertex intersection(vertex v1,vertex v2,int side)
{
    vertex output;
    float slope = (v2.y-v1.y)/(v2.x-v1.x);
    if(side == 0)
    {
        output.x = border[0].x;
        output.y = v1.y + (border[0].x - v1.x)*slope;
    }
    else if(side == 1)
    {
        output.x = border[1].x;
        output.y = v1.y + (border[1].x - v1.x)*slope;
    }
    else if(side == 2)
    {
        output.y = border[0].y;
        output.x = v1.x + (border[0].y - v1.y)/slope;
    }
    else if(side == 3)
    {
        output.y = border[2].y;
        output.x = v1.x + (border[2].y - v1.y)/slope;
    }
    return output;
}
void clip(vertex* poly,int* count)
{
    vertex v1,v2;
    vertex cord[10];
    int out=0;
    for(int i=0;i<4;i++)
    {
        out = 0;
        for(int j=0;j<(*count);j++)
        {
            v1 = poly[j];
            v2 = poly[(j+1)% *count];
            if(isIn(v1,i))
            {
                if(isIn(v2,i))
                {
                    cord[out++] = v2;
                }
                else
                {
                    cord[out++] = intersection(v1,v2,i);
                }
            }
            else
            {
                if(isIn(v2,i))
                {
                    cord[out++] = intersection(v1,v2,i);
                    cord[out++] = v2;
                }
            }
        }
        for(int k=0;k<out;k++)
            poly[k] = cord[k];
        *count = out;
    }
}
void display()
{

    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,0,0);
    draw_polygon(border,4);

    glColor3f(0,1,0);
    draw_polygon(polygon,vcount);

    glFlush();

}
void mouse(int btn,int state,int x,int y)
{
    if(btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        clip(polygon,&vcount);
        glutPostRedisplay();
    }
}
void init()
{
    glPointSize(5);
    glClearColor(1,1,1,1);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);

    border[0].x = 100,border[0].y = 100;
    border[1].x = 300,border[1].y = 100;
    border[2].x = 300,border[2].y = 300;
    border[3].x = 100,border[3].y = 300;

    polygon[0].x = 50, polygon[0].y = 200;
    polygon[1].x = 200, polygon[1].y = 50;
    polygon[2].x = 350, polygon[2].y = 200;
    polygon[3].x = 200, polygon[3].y = 350;

    vcount = 4;

}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Polygon Clipping");
    init();
    glutDisplayFunc(display);
    glutMouseFunc(mouse);
    glutMainLoop();
    return 0;
}
