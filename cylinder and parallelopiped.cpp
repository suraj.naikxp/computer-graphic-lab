#define _USE_MATH_DEFINES               //Used to get the value of PI directly
#include<GL/glut.h>
#include<cmath>
int xc = 120,yc=120,r=100;
int o = 0;
void circle()
{
    glColor3f(1,0,0);
    glBegin(GL_LINE_LOOP);
    for(int i=0;i<360;i++)
    {
        glVertex2f(o+xc+r*cos(i*M_PI/180),o+yc+r*sin(i*M_PI/180));          //M_PI comes from _USE_MATH_DEFINES, directly use 3.14 if you feel like
    }
    glEnd();
}
void paral()
{
    glColor3f(0,1,0);
    glBegin(GL_LINE_LOOP);
    glVertex2f(o+100,o+300);
    glVertex2f(o+100,o+400);
    glVertex2f(o+150,o+400);
    glVertex2f(o+150,o+300);
    glEnd();
}
void display()
{
    for(int i=0;i<100;i++)
    {
        o++;
        circle();
        paral();
    }
    glFlush();
}
void init()
{
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Hello World");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
