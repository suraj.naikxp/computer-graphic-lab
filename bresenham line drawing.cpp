#include<GL/glut.h>
#include<iostream>
using namespace std;
int x1,y1,x2,y2;
void showPoint(int x,int y)
{
    glBegin(GL_POINTS);
    glVertex2i(x,y);
    glEnd();
}
void display()
{
    int dx,dy,inc1,inc2,incx,incy,x,y,e;
    dx = x2-x1;
    dy = y2-y1;
    if(dy<0)
    {
        dy = -dy;
        incy = -1;
    }
    else
        incy = 1;
    if(dx<0)
    {
        dx = -dx;
        incx = -1;
    }
    else
        incx = 1;

    x = x1;
    y = y1;
    if(dx>dy)
    {
        showPoint(x,y);
        e = 2* dy - dx;
        inc1 = 2*dy - 2*dx;
        inc2 = 2*dy;
        for(int i=0;i<dx;i++)
        {
            if(e>=0)
            {
                e+=inc1;
                y+=incy;
            }
            else
            {
                e+=inc2;
            }
            x+=incx;
            showPoint(x,y);
        }
    }
    else
    {
        showPoint(x,y);
        e = 2*dx-dy;
        inc1 = 2*dx - 2*dy;
        inc2 = 2*dx;
        for(int i=0;i<dy;i++)
        {
            if(e>=0)
            {
                e+=inc1;
                x+=incx;
            }
            else
            {
                e+=inc2;
            }
            y+=incy;
            showPoint(x,y);
        }
    }
    glFlush();
}
void init()
{
    glColor3f(0,0,0);
    glClearColor(1,1,1,1);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,600,0,600);
    glMatrixMode(GL_MODELVIEW);
}
int main(int argc,char** argv)
{
    cin>>x1>>y1>>x2>>y2;
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Hello World");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
}
